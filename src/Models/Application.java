/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import config.UtilHibernate;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author PRAKTIKUM
 */
public class Application {

    private List<Mahasiswa> listMahasiswa = new ArrayList<>();
    
    private Session session = UtilHibernate.getSessionFactory().openSession();
    

    public Transaction getTransaction() {
        return session.getTransaction();
    }
    
    

    public boolean insertMahasiswa(Mahasiswa m) {
        try {
            
            getTransaction().begin();
            session.save(m);
            getTransaction().commit();

            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new IllegalStateException(e.getLocalizedMessage());

        }
    }

    public List<Mahasiswa> getAll() {
        
        Query query = session.createQuery("From Mahasiswa");
      
        return query.list();
    }
    
    public boolean deleteMahasiswa(Mahasiswa m)
    {
        try
        {
            getTransaction().begin();
            session.delete(m);
            getTransaction().commit();
            return true;
        } catch (Exception e)
        {
            e.printStackTrace();
            throw new IllegalStateException(e.getLocalizedMessage());
        }
    }
    
    public Mahasiswa getMahasiswaByNim(String nim)
    {
        return (Mahasiswa) session.get(Mahasiswa.class, nim);
    }

}
